# HM outlier detection

Outlier detection based on the minimal radius of a dense area

Exemple at iris dataset:
![alt text](https://gitlab.com/gabrielhooper/hm-outlier-detection/raw/master/outlier.png "Exemplo de detecção de outlier: Iris dataset")

Datasets:
    http://odds.cs.stonybrook.edu/