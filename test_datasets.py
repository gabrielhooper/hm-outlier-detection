from hm_outlier import hm_outlier
import pandas as pd
# data = pd.read_csv('./datasets/lymphography.csv',header=None)
# lymp = hm_outlier(distance='euc',cutoff=2,n_points=1,approach='n')
# lymp.fit(data = data)
# print(str(len(lymp.outliers_))+" Outliers found\n\n")

data = pd.read_csv('./datasets/glass.csv',header=None)
#print(data.head())
data = data.drop(0,axis=1)
#print(data.head())
wbc = hm_outlier(distance='euc',cutoff=2,n_points=1,approach='n')
wbc.fit(data = data)
print(str(len(wbc.outliers_index_))+" Outliers found")
wbc.pair_plot(figsize=[20,20])