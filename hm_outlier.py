#-*- coding:utf-8 -*-
import numpy as np
import pandas as pd
from scipy.spatial import distance_matrix
import seaborn as sns
import matplotlib.pyplot as plt

class hm_outlier():
    radius_ = [] #All maximum radiuses
    outliers_ = [] #outliers true/false
    outliers_index_ = []
    data = []
    n_points = 1
    distance = 'euc' #for euclidian distance or 'maho'
    approach = 'n' #for normal approach or 'p' for percentil approach
    cutoff = 3 #value for cutoff (for normal or percentil approach)

    accepted_distance = ['euc']#,'maho']
    accepted_approach = ['n','p']#n=>normal distribution, p=>percentil approach

    def __init__(self,n_points=1,distance='euc',approach='n',cutoff='3'):
        
        if n_points>0 and type(n_points)== int:
            self.n_points=n_points
        else:
            raise Exception('n_points must be a non zero positive integer value')
        if distance in self.accepted_distance:
            self.distance = distance
        else:
            raise Exception('Distance not accepted')
        if approach in self.accepted_approach:
            self.approach = approach
        else:
            raise Exception('Approach not accepted')
        if cutoff>0:
            self.cutoff = cutoff
        else:
            raise Exception('cutoff must be a non zero positive value')
        self.outliers_ = [] #outliers true/false
        self.outliers_index_ = []
        self.radius_ = []
        self.data = []
    
    def fit(self,data=[]):
        #print(type(data))
        if len(data)>0 and (type(data) == np.ndarray or type(data) == pd.core.frame.DataFrame):
            if type(data) == pd.core.frame.DataFrame:
                self.data = data
            else:
                self.data = pd.DataFrame(data)
        else:
            raise Exception('Data not valid. Must not be empty and be np.array or pd.DataFrame')
        self.radius_ =  np.ones(len(data))

        if self.distance == 'euc':        
            #calculate distance
            distance_m = distance_matrix(data,data) ##O(n²), can be O(n²/2) if better treated
            #print(distance_m)
            #get minimal radius
            for i,dist_row in enumerate(distance_m):
                self.radius_[i]=np.partition(dist_row, self.n_points)[0:self.n_points+1].max()
                #print(np.partition(dist_row, self.n_points)[0:self.n_points+1])
            #print(self.radius_)

        if self.approach == 'n':
            #calculate mean and std
            mean_radius = self.radius_.mean()
            std_radius = self.radius_.std()
            #find indexes of values bigger than mean + cutoff*std
            #print(mean_radius)
            #print(std_radius)
            #print(mean_radius + self.cutoff*std_radius)
            for i,r in enumerate(self.radius_):
                if r > mean_radius + self.cutoff*std_radius:
                    self.outliers_index_.append(i)
                    self.outliers_.append('True')
                else:
                    self.outliers_.append('False')
                    #print(i)
                    #print(r)
            #print(self.outliers_)
        else:
            pass
            #calculate IQD and 3rd quartil

            #Calculate cutoff*IQD + 3rd quartil

    def distance_distribution(self, plot='dist'):
        if plot=='dist':
            plt.figure()
            sns.distplot(self.radius_)
            plt.show()
        else:
            plt.figure()
            sns.boxplot(self.radius_)
            plt.show()

    def pair_plot(self,figsize = [15,10],fontsize=10):
        '''
        Pair plot all scatter plots with outliers separated by color and numbered by index
        '''
        #print(self.data.head())
        plt.figure(figsize=figsize) #choose figsize
        k=0 #number of subplot
        for i in range(self.data.shape[1]):
            for j in range(self.data.shape[1]):
                k=k+1
                plt.subplot(self.data.shape[1],self.data.shape[1],k)
                if j==i:
                    #print(self.data.iloc[:,i])
                    sns.distplot(self.data.iloc[:,i],axlabel=False) #histogram
                else:
                    pl = sns.scatterplot(self.data.iloc[:,j],self.data.iloc[:,i],hue=self.outliers_,legend=None)
                    for out in self.outliers_index_: #annotate all outlier indexes
                        #print(out)
                        x = self.data.iloc[out,j]
                        #print(x)
                        y = self.data.iloc[out,i]
                        pl.text(x, y, str(out), horizontalalignment='left', fontsize=fontsize, color='black', weight='semibold')
        plt.show()
            

    def remove_outliers(self):
        pass
    

    def printar(self):
        print(str(data))
        print('n_points: '+str(self.n_points))
        print('distance: '+str(self.distance))
        print('approach: '+str(self.approach))
        print('cutoff: '+str(self.cutoff))
        

# #data = np.array([[5,1],[1,5],[4,0],[0,4],[2,2],[5,5]])
# #data = pd.DataFrame(data)
# a = hm_outlier(distance='euc',cutoff=2,n_points=1,approach='n')
# plt.figure()
# sns.scatterplot(x=data[:,0],y=data[:,1])
# plt.show()
# a.fit(data = data)
# #a.printar()
# a.show_distribution(plot='dist')

